﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {
	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();

	}
	
	// Update is called once per frame
	void Update () {
		rigidbody.velocity = speed * direction;
	
	}
	void onCollisionEnter(Collision collision) {
		Destroy (gameObject);
	}
}
